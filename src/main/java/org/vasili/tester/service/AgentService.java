package org.vasili.tester.service;

import org.springframework.stereotype.Service;
import org.vasili.tester.DTO.ProviderDTO;

@Service
public interface AgentService {

    ProviderDTO getAgent(long id, Long bankId);
}
