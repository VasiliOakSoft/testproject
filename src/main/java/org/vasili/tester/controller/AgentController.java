package org.vasili.tester.controller;

import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.vasili.tester.DTO.ProviderDTO;
import org.vasili.tester.DTO.MyExceptionDTO;
import org.vasili.tester.service.AgentService;


@Controller
@RequestMapping("/")
public class AgentController {

    AgentService agentService;

    @RequestMapping(value = "agents/{id}/providerBankInfo", method = RequestMethod.GET)
    @ResponseBody
    public ProviderDTO agentSearchGet(@PathVariable("id") long id, @RequestParam(value = "bankId", required = false) Long bankId) throws Exception {

        return agentService.getAgent(id, bankId);
    }

    @RequestMapping(value = "agents/{id}/providerBankInfo/search", method = RequestMethod.POST)
    @ResponseBody
    public ProviderDTO agentSearchPost(@PathVariable("id") long id, @RequestParam(value = "bankId", required = false) Long bankId) throws Exception {

        return agentService.getAgent(id, bankId);
    }


    @RequestMapping(value = "**", method = {RequestMethod.GET, RequestMethod.POST})
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public MyExceptionDTO handleError(HttpServletRequest req, Exception exception) throws Exception {

        return new MyExceptionDTO(req.getRequestURI(), "404 Error.");
    }

}