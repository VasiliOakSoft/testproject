package org.vasili.tester.model;

public class BankInfo {
    private Long bankId;
    private String name;
    private long correspondentAccount;
    private String address;
    private boolean isClosed;
    private long INN;
    private long BIK;

    public BankInfo()
    {

    }

    public BankInfo(Long bankId, String name, long correspondentAccount, String address, boolean isClosed, long INN, long BIK)
    {
        this.bankId = bankId;
        this.name = name;
        this.correspondentAccount = correspondentAccount;
        this.address = address;
        this.isClosed = isClosed;
        this.INN = INN;
        this.BIK = BIK;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCorrespondentAccount() {
        return correspondentAccount;
    }

    public void setCorrespondentAccount(long correspondentAccount) {
        this.correspondentAccount = correspondentAccount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    public long getINN() {
        return INN;
    }

    public void setINN(long INN) {
        this.INN = INN;
    }

    public long getBIK() {
        return BIK;
    }

    public void setBIK(long BIK) {
        this.BIK = BIK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankInfo bankInfo = (BankInfo) o;

        if (correspondentAccount != bankInfo.correspondentAccount) return false;
        if (isClosed != bankInfo.isClosed) return false;
        if (INN != bankInfo.INN) return false;
        if (BIK != bankInfo.BIK) return false;
        if (!bankId.equals(bankInfo.bankId)) return false;
        if (name != null ? !name.equals(bankInfo.name) : bankInfo.name != null) return false;
        return address != null ? address.equals(bankInfo.address) : bankInfo.address == null;

    }

    @Override
    public int hashCode() {
        int result = bankId.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (correspondentAccount ^ (correspondentAccount >>> 32));
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (isClosed ? 1 : 0);
        result = 31 * result + (int) (INN ^ (INN >>> 32));
        result = 31 * result + (int) (BIK ^ (BIK >>> 32));
        return result;
    }
}
