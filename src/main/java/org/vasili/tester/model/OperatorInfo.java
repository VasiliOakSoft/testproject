package org.vasili.tester.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OperatorInfo {

    @JsonIgnore
    private long operatorId;

    private long recipientAccount;
    private long INN;
    private long KPP;

    public OperatorInfo()
    {

    }

    public OperatorInfo(long operatorId, long recipientAccount, long INN, long KPP)
    {
        this.operatorId = operatorId;
        this.recipientAccount = recipientAccount;
        this.INN = INN;
        this.KPP = KPP;
    }


    @JsonIgnore
    public long getOperatorId() {
        return operatorId;
    }

    @JsonIgnore
    public void setOperatorId(long operatorId) {
        this.operatorId = operatorId;
    }

    public long getRecipientAccount() {
        return recipientAccount;
    }

    public void setRecipientAccount(long recipientAccount) {
        this.recipientAccount = recipientAccount;
    }

    public long getINN() {
        return INN;
    }

    public void setINN(long INN) {
        this.INN = INN;
    }

    public long getKPP() {
        return KPP;
    }

    public void setKPP(long KPP) {
        this.KPP = KPP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OperatorInfo that = (OperatorInfo) o;

        if (recipientAccount != that.recipientAccount) return false;
        if (INN != that.INN) return false;
        return KPP == that.KPP;

    }

    @Override
    public int hashCode() {
        int result = (int) (recipientAccount ^ (recipientAccount >>> 32));
        result = 31 * result + (int) (INN ^ (INN >>> 32));
        result = 31 * result + (int) (KPP ^ (KPP >>> 32));
        return result;
    }
}
