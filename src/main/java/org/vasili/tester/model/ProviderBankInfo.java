package org.vasili.tester.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProviderBankInfo {


    @JsonIgnore
    private long agentId;

    private OperatorInfo operatorInfo;
    private BankInfo bankInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderBankInfo that = (ProviderBankInfo) o;

        if (!operatorInfo.equals(that.operatorInfo)) return false;
        return bankInfo.equals(that.bankInfo);

    }

    @Override
    public int hashCode() {
        int result = operatorInfo.hashCode();
        result = 31 * result + bankInfo.hashCode();
        return result;
    }

    public ProviderBankInfo()
    {

    }

    public ProviderBankInfo(long agentId, OperatorInfo operatorInfo, BankInfo bankInfo)
    {
        this.agentId = agentId;
        this.operatorInfo = operatorInfo;
        this.bankInfo = bankInfo;
    }

    @JsonIgnore
    public long getAgentId() {
        return agentId;
    }

    @JsonIgnore
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public OperatorInfo getOperatorInfo() {
        return operatorInfo;
    }

    public void setOperatorInfo(OperatorInfo operatorInfo) {
        this.operatorInfo = operatorInfo;
    }

    public BankInfo getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(BankInfo bankInfo) {
        this.bankInfo = bankInfo;
    }
}
