package org.vasili.tester.DTO;

import org.vasili.tester.model.ProviderBankInfo;

public class ProviderDTO {

    private ProviderBankInfo providerBankInfo;

    public ProviderDTO()
    {

    }

    public ProviderDTO(ProviderBankInfo providerBankInfo)
    {
        this.providerBankInfo = providerBankInfo;
    }

    public ProviderBankInfo getProviderBankInfo() {
        return providerBankInfo;
    }

    public void setProviderBankInfo(ProviderBankInfo providerBankInfo) {
        this.providerBankInfo = providerBankInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderDTO that = (ProviderDTO) o;

        return providerBankInfo.equals(that.providerBankInfo);

    }

    @Override
    public int hashCode() {
        return providerBankInfo.hashCode();
    }
}
