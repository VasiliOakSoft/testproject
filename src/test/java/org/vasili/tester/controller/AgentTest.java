package org.vasili.tester.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.vasili.tester.DTO.ProviderDTO;
import org.vasili.tester.config.WebAppConfig;
import org.vasili.tester.DTO.MyExceptionDTO;
import org.vasili.tester.model.BankInfo;
import org.vasili.tester.model.OperatorInfo;
import org.vasili.tester.model.ProviderBankInfo;
import org.vasili.tester.service.AgentService;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WebAppConfig.class})
public class AgentTest {

    @Autowired
    WebApplicationContext wac;

    @Autowired
    private AgentService agentService;

    private AgentController agentController;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;
    private ProviderDTO providerDTO;
    private MyExceptionDTO myExceptionDTO;


    @org.junit.Before
    public void setUp() throws Exception {

        agentController = new AgentController();
        agentService = mock(AgentService.class);
        ReflectionTestUtils.setField(agentController, "agentService", agentService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(agentController).build();
        objectMapper = new ObjectMapper();
        providerDTO = new ProviderDTO(new ProviderBankInfo(1L,new OperatorInfo(2L, 3L, 4L, 5L), new BankInfo(6L, "7", 8L, "9", true, 10L, 11L)));
        when(agentController.agentService.getAgent(1L, null)).thenReturn(providerDTO);
        when(agentController.agentService.getAgent(1L, 2L)).thenReturn(providerDTO);
        when(agentController.agentService.getAgent(1L, 22L)).thenReturn(providerDTO);

    }

    @org.junit.After
    public void tearDown() throws Exception {

    }

    @Test
    public void TestURLWrongWayMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/qweqwe")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/qweqwe", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        verifyZeroInteractions(agentService);
        assertEquals(myExceptionDTO, p);
    }

    @Test
    public void TestURLWrongWayMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/qweqwer")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/qweqwer", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        verifyZeroInteractions(agentService);
        assertEquals(myExceptionDTO, p);
    }

    @Test
    public void TestAgentsNoAgentIdMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/agents//providerBankInfo")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/agents/providerBankInfo", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        verifyZeroInteractions(agentService);
        assertEquals(myExceptionDTO, p);
    }

    @Test
    public void TestAgentsNoAgentIdMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/agents//providerBankInfo")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/agents/providerBankInfo", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        verifyZeroInteractions(agentService);
        assertEquals(myExceptionDTO, p);
    }

    @Test
    public void TestAgentsWrongAgentIdMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/agents/qwe/providerBankInfo")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/agents/qwe/providerBankInfo", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        verifyZeroInteractions(agentService);
        assertEquals(myExceptionDTO, p);
    }

    @Test
    public void TestAgentsWrongAgentIdMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/agents/qwe/providerBankInfo")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/agents/qwe/providerBankInfo", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        verifyZeroInteractions(agentService);
        assertEquals(myExceptionDTO, p);
    }

    @Test
    public void TestAgentsRightAgentIdMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/agents/1/providerBankInfo")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();

        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, null);
        verifyNoMoreInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/agents/1/providerBankInfo/search")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, null);
        verifyNoMoreInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdRightBankIdMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/agents/1/providerBankInfo?bankId=2")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, 2L);
        verifyNoMoreInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdRightBankIdMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/agents/1/providerBankInfo/search").param("bankId", "2")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, 2L);
        verifyNoMoreInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdWrongBankIdMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/agents/1/providerBankInfo?bankId=qwe")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/agents/1/providerBankInfo", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        assertEquals(myExceptionDTO, p);
        verifyZeroInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdWrongBankIdMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/agents/1/providerBankInfo/search").param("bankId", "qwew")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("url").exists())
                .andExpect(jsonPath("message").exists())
                .andReturn();
        myExceptionDTO = new MyExceptionDTO("/agents/1/providerBankInfo/search", "404 Error.");
        MyExceptionDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), MyExceptionDTO.class);
        assertEquals(myExceptionDTO, p);
        verifyZeroInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdBadParameterMethodGet() throws Exception
    {
        when(agentService.getAgent(anyLong(), anyLong())).thenReturn(providerDTO);

        MvcResult result =this.mockMvc.perform(get("/agents/1/providerBankInfo?bank=22")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, null);
        verifyNoMoreInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdBadParameterMethodPost() throws Exception
    {
        when(agentService.getAgent(anyLong(), anyLong())).thenReturn(providerDTO);

        MvcResult result =this.mockMvc.perform(post("/agents/1/providerBankInfo/search").param("bank", "2")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, null);
        verifyNoMoreInteractions(agentService);
    }
    @Test
    public void TestAgentsRightAgentIdMoreParamsMethodGet() throws Exception
    {
        MvcResult result =this.mockMvc.perform(get("/agents/1/providerBankInfo?bankId=22&ppp=5")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, 22L);
        verifyNoMoreInteractions(agentService);
    }

    @Test
    public void TestAgentsRightAgentIdMoreParamsMethodPost() throws Exception
    {
        MvcResult result =this.mockMvc.perform(post("/agents/1/providerBankInfo/search").param("bankId", "22").param("ppp", "5")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("providerBankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.recipientAccount").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.operatorInfo.kpp").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bankId").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.name").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.correspondentAccount").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.address").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.isClosed").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.inn").exists())
                .andExpect(jsonPath("providerBankInfo.bankInfo.bik").exists())
                .andReturn();
        ProviderDTO p = objectMapper.readValue(result.getResponse().getContentAsString(), ProviderDTO.class);
        assertEquals(providerDTO, p);
        verify(agentService, times(1)).getAgent(1L, 22L);
        verifyNoMoreInteractions(agentService);
    }

}